From "variouswalkcycle.png"
![](https://github.com/izz-j/mariko/raw/master/sprites-preview.png)

Tiles by artofsully
![](https://github.com/izz-j/mariko/raw/master/hex4.gif)

closed source project character "The Sorceress" by artofsully
![](https://github.com/izz-j/mariko/raw/master/sample_sorceress.gif)

From "variouswalkcycle.png"
![](https://github.com/izz-j/mariko/raw/master/sample-anim.gif)

My experimental sprite renderer.
The characters on the "variouswalkcycle.png" are open and are from [opengameart](https://opengameart.org/)


**Instructions**

Clone or extract sgl-2d to your local projects directory "~/quicklisp/local-projects/"

then quickload sgl

(ql:quickload 'sgl)

To run mariko's examples, first load the system

(asdf:load-system 'sgl/sgl-examples)

then run an example

(sgl-examples:basic-sprite-test)

(sgl-examples:anim-test)

(sgl-examples:tile-map-test)
