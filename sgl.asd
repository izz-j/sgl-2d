;;;; sgl.asd

(asdf:defsystem #:sgl
  :description "sprite tools"
  :author "Iseman Johnson"
  :license "MIT"
  :serial t
  :pathname "src"
  :depends-on (:cl-opengl :sdl2 :sdl2-image :cl-glfw3)
  :components ((:file "package")
               (:file "sgl")))

;;;SGL examples
(asdf:defsystem #:sgl/sgl-examples
  :description "sprite examples"
  :license "MIT"
  :serial t
  :pathname "examples"
  :depends-on (:sgl)
  :components ((:file "package")
	       (:file "tile-map")
	       (:file "anim")
	       (:file "basic-sprite")))

