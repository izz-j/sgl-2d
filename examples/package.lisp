(defpackage #:sgl-examples
  (:use #:cl)
  (:export #:tile-map-test
	   #:basic-sprite-test
	   #:anim-test))
